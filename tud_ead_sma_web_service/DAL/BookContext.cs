﻿using Microsoft.EntityFrameworkCore;
using tud_ead_sma_web_service.Models;


namespace tud_ead_sma_web_service.DAL
{
    public partial class BookContext : DbContext
    {
        // Constructor accepting a DbContextOptions
        // For allowing to pass the DbContextOptions when instantiating a context (Cf. Startup.cs)
        public BookContext(DbContextOptions<BookContext> options) : base(options)
        { }

        public DbSet<Author> Authors { get; set; }
        public DbSet<Book> Books { get; set; }

        // Seed initial data
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Authors
            modelBuilder.Entity<Author>().HasData(new Author { AuthorID = 1, AuthorName = "William Shakespeare", AuthorNationality = "English" });
            modelBuilder.Entity<Author>().HasData(new Author { AuthorID = 2, AuthorName = "Samuel Beckett", AuthorNationality = "Irish" });
            modelBuilder.Entity<Author>().HasData(new Author { AuthorID = 3, AuthorName = "Emile Zola", AuthorNationality = "French" });

            // Books (with related Authors)
            modelBuilder.Entity<Book>().HasData(
                new Book { BookID = 1, AuthorID = 1, BookTitle = "Hamlet", BookYear = 1600 },
                new Book { BookID = 2, AuthorID = 1, BookTitle = "King Lear", BookYear = 1606 },
                new Book { BookID = 3, AuthorID = 1, BookTitle = "Othello", BookYear = 1622 }
            );
            modelBuilder.Entity<Book>().HasData(
                new Book { BookID = 4, AuthorID = 2, BookTitle = "Waiting for Godot", BookYear = 1953 },
                new Book { BookID = 5, AuthorID = 2, BookTitle = "Murphy", BookYear = 1938 },
                new Book { BookID = 6, AuthorID = 2, BookTitle = "Watt", BookYear = 1953 }
            );
            modelBuilder.Entity<Book>().HasData(
                new Book { BookID = 7, AuthorID = 3, BookTitle = "Au Bonheur des Dames", BookYear = 1883 },
                new Book { BookID = 8, AuthorID = 3, BookTitle = "Nana", BookYear = 1880 },
                new Book { BookID = 9, AuthorID = 3, BookTitle = "Germinal", BookYear = 1885 }
            );

        }
    }
}
