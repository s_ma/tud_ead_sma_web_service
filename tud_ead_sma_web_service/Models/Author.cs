﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tud_ead_sma_web_service.Models
{
    [Table("Author")]
    public class Author
    {
        [Key]
        public int AuthorID { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 1)]
        public string AuthorName { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 1)]
        public string AuthorNationality { get; set; }

        public virtual ICollection<Book> Books { get; set; }
    }
}
