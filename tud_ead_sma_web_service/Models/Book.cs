﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tud_ead_sma_web_service.Models
{
    [Table("Book")]
    public class Book
    {
        [Key]
        public int BookID { get; set; }

        [Required]
        public int AuthorID { get; set; }

        [Required]
        [StringLength(150, MinimumLength = 1)]
        public string BookTitle { get; set; }

        [Required]
        [Range(-4000, 3000)]
        public int BookYear { get; set; }

        [ForeignKey("AuthorID")]
        public virtual Author Author { get; set; }
    }
}
