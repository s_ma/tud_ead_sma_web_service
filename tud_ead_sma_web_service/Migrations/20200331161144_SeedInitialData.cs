﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace tud_ead_sma_web_service.Migrations
{
    public partial class SeedInitialData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Author",
                columns: new[] { "AuthorID", "AuthorName", "AuthorNationality" },
                values: new object[] { 1, "William Shakespeare", "English" });

            migrationBuilder.InsertData(
                table: "Author",
                columns: new[] { "AuthorID", "AuthorName", "AuthorNationality" },
                values: new object[] { 2, "Samuel Beckett", "Irish" });

            migrationBuilder.InsertData(
                table: "Author",
                columns: new[] { "AuthorID", "AuthorName", "AuthorNationality" },
                values: new object[] { 3, "Emile Zola", "French" });

            migrationBuilder.InsertData(
                table: "Book",
                columns: new[] { "BookID", "AuthorID", "BookTitle", "BookYear" },
                values: new object[,]
                {
                    { 1, 1, "Hamlet", 1600 },
                    { 2, 1, "King Lear", 1606 },
                    { 3, 1, "Othello", 1622 },
                    { 4, 2, "Waiting for Godot", 1953 },
                    { 5, 2, "Murphy", 1938 },
                    { 6, 2, "Watt", 1953 },
                    { 7, 3, "Au Bonheur des Dames", 1883 },
                    { 8, 3, "Nana", 1880 },
                    { 9, 3, "Germinal", 1885 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Book",
                keyColumn: "BookID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Book",
                keyColumn: "BookID",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Book",
                keyColumn: "BookID",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Book",
                keyColumn: "BookID",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Book",
                keyColumn: "BookID",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Book",
                keyColumn: "BookID",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Book",
                keyColumn: "BookID",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Book",
                keyColumn: "BookID",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Book",
                keyColumn: "BookID",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Author",
                keyColumn: "AuthorID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Author",
                keyColumn: "AuthorID",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Author",
                keyColumn: "AuthorID",
                keyValue: 3);
        }
    }
}
