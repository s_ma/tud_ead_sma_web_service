﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using tud_ead_sma_web_service.DAL;

namespace tud_ead_sma_web_service.Migrations
{
    [DbContext(typeof(BookContext))]
    partial class BookContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.3")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("tud_ead_sma_web_service.Models.Author", b =>
                {
                    b.Property<int>("AuthorID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("AuthorName")
                        .IsRequired()
                        .HasColumnType("nvarchar(100)")
                        .HasMaxLength(100);

                    b.Property<string>("AuthorNationality")
                        .IsRequired()
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.HasKey("AuthorID");

                    b.ToTable("Author");

                    b.HasData(
                        new
                        {
                            AuthorID = 1,
                            AuthorName = "William Shakespeare",
                            AuthorNationality = "English"
                        },
                        new
                        {
                            AuthorID = 2,
                            AuthorName = "Samuel Beckett",
                            AuthorNationality = "Irish"
                        },
                        new
                        {
                            AuthorID = 3,
                            AuthorName = "Emile Zola",
                            AuthorNationality = "French"
                        });
                });

            modelBuilder.Entity("tud_ead_sma_web_service.Models.Book", b =>
                {
                    b.Property<int>("BookID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("AuthorID")
                        .HasColumnType("int");

                    b.Property<string>("BookTitle")
                        .IsRequired()
                        .HasColumnType("nvarchar(150)")
                        .HasMaxLength(150);

                    b.Property<int>("BookYear")
                        .HasColumnType("int");

                    b.HasKey("BookID");

                    b.HasIndex("AuthorID");

                    b.ToTable("Book");

                    b.HasData(
                        new
                        {
                            BookID = 1,
                            AuthorID = 1,
                            BookTitle = "Hamlet",
                            BookYear = 1600
                        },
                        new
                        {
                            BookID = 2,
                            AuthorID = 1,
                            BookTitle = "King Lear",
                            BookYear = 1606
                        },
                        new
                        {
                            BookID = 3,
                            AuthorID = 1,
                            BookTitle = "Othello",
                            BookYear = 1622
                        },
                        new
                        {
                            BookID = 4,
                            AuthorID = 2,
                            BookTitle = "Waiting for Godot",
                            BookYear = 1953
                        },
                        new
                        {
                            BookID = 5,
                            AuthorID = 2,
                            BookTitle = "Murphy",
                            BookYear = 1938
                        },
                        new
                        {
                            BookID = 6,
                            AuthorID = 2,
                            BookTitle = "Watt",
                            BookYear = 1953
                        },
                        new
                        {
                            BookID = 7,
                            AuthorID = 3,
                            BookTitle = "Au Bonheur des Dames",
                            BookYear = 1883
                        },
                        new
                        {
                            BookID = 8,
                            AuthorID = 3,
                            BookTitle = "Nana",
                            BookYear = 1880
                        },
                        new
                        {
                            BookID = 9,
                            AuthorID = 3,
                            BookTitle = "Germinal",
                            BookYear = 1885
                        });
                });

            modelBuilder.Entity("tud_ead_sma_web_service.Models.Book", b =>
                {
                    b.HasOne("tud_ead_sma_web_service.Models.Author", "Author")
                        .WithMany("Books")
                        .HasForeignKey("AuthorID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
